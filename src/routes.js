import { createRouter, createWebHistory } from 'vue-router'
import Login from './components/Login.vue'
import Register from "./components/Register.vue";
import Costume from "./components/Costume.vue";
import LandingPage from './components/Landingpage.vue';
const routes = [
    // { path: '/login', component: Login },
    // { path: '/register', component: Register },
    { path: '/', component: LandingPage },
    { path: '/costume', component: Costume }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router