import { ref } from 'vue'
import { supabase } from '../supabase.js'

const email = ref('');
const password = ref('');
const first_name = ref('');
const last_name = ref('');

const register = async () => {
    try {
        const {data, error} = await supabase.from('users').insert([
            {email: email.value, first_name: first_name.value, last_name: last_name.value, costume: costume}
        ])
        if (error) throw error
        // setTimeout(() => {
        //     window.location.href = '/login'
        // }, 1000)
    } catch (error) {
        console.error(error.error_description || error.message)
    }
}

const login = async () => {
    try {
        const { user, error } = await supabase.auth.signInWithPassword({ email: email.value, password: password.value })
        if (error) throw error
        // console.log('login')
        window.location.href = '/'
    } catch (error) {
        console.error("Error : " + error.message)
    }
}

const logout = async () => {
    try {
        const { error } = await supabase.auth.signOut()
        if (error) throw error
        // console.log('logout')
        window.location.href = '/login'
    } catch (error) {
        console.error("Error : " + error.message)
    }

}

export function Auth() {
    return {
        email,
        password,
        first_name,
        last_name,
        register,
        login,
        logout
    }
}