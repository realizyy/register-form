import { ref } from 'vue'
import { supabase } from '../supabase.js'

const nama = ref('');
const division = ref('');
const costume = ref('');
const list_costume = ['Pajamas', 'Animal Print', 'Floral', 'Sarong', 'Wierd Outfit'];

export const registerEvent = async (nama, divisi) => {
    const ID = await supabase.from('register').select('id').order('id', {ascending: false}).limit(1)
    if (ID.data.length === 0) {
        costume.value = list_costume[0]
    } else {
        const lastID = ID.data[0].id
        if (lastID === 5) {
            costume.value = 'Pajamas'
        } else {
            const index = lastID % 5
            costume.value = list_costume[index]
        }
    }
    const {data, error} = await supabase.from('register').insert([
        {name: nama, division: divisi, costume: costume.value}
    ])
    // console.log('Name : ' + nama, '\nDivision : ' + divisi, '\nCostume : ' + costume.value)
    if (error) {
        console.log('Error:\n', error)
    }
    return costume.value;
}

export const  getRegister = async () => {
    const {data, error} = await supabase.from('register').select('*')
    if (error) {
        console.log('Error:\n', error)
    }
    return data
}

export const getCostume = async () => {
    return costume.value
}

export function Register() {
    return {
        name,
        division,
        costume,
        list_costume,
        registerEvent,
        getRegister
    }
}