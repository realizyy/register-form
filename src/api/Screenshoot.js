import html2canvas from 'html2canvas';

export const screenshotPage = async () => {
  const element = document.body;

  const canvas = await html2canvas(element);

  const imgData = canvas.toDataURL('image/png');

  const link = document.createElement('a');
  link.href = imgData;
  link.download = 'screenshot_wtid_party.png';

  document.body.appendChild(link);

  link.click();

  document.body.removeChild(link);
};